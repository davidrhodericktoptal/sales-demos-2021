import React, {useEffect} from 'react'
import {Row, Col, notification} from 'antd'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import {useSelector, useDispatch} from 'react-redux'

import {setTransactionFee} from './components/orderSlice'

import TopBar from './components/top-bar'
import CartHeader from './components/cart-header'

import Content from './components/content'
import OrderSummary from './components/order-summary'

const App = () => {
  const order = useSelector(state => state.order)
  const {subtotal, surchargeableSubtotal, delivery, tax, paymentInformation, merchantControls} = order
  const {creditCard, postalCode} = paymentInformation

  const dispatch = useDispatch()

  useEffect(() => {
    if(creditCard.substr(0, 6).length === 6 &&
       postalCode.length === 5 &&
       // eslint-disable-next-line
       ((merchantControls.activeMerchantRule === 'demo-no-rules' && surchargeableSubtotal > 0) || 
       // eslint-disable-next-line
       merchantControls.activeMerchantRule !== 'demo-no-rules' && subtotal > 0)) {
       const data = {
        nicn          : creditCard.substr(0, 6),
        amount        : merchantControls.activeMerchantRule === 'demo-no-rules' ? parseFloat(surchargeableSubtotal + delivery + tax).toFixed(2) : parseFloat(subtotal + delivery + tax).toFixed(2),
        region        : postalCode,
        processor     : merchantControls.activeProcessor
      }

      const bearerTokenVar = `REACT_APP_${merchantControls.activeMerchantRule.toUpperCase().replaceAll('-', '_')}`

      fetch('https://api.interpayments.com/v1/ch', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${process.env[bearerTokenVar]}`
        },
        body: JSON.stringify(data),
        cache: 'no-store'
      }).then(res => res.json())
        .then(result => {
          dispatch(setTransactionFee(result.transactionFee))
        }, error => {
          dispatch(setTransactionFee(0))
          notification['error']({
            message: 'API Error',
            description: error.toString()
          })
          console.error(error.toString())
        })
    } else {
      dispatch(setTransactionFee(0))
    }
  }, [creditCard, surchargeableSubtotal, postalCode, delivery, tax, dispatch, merchantControls, subtotal])

  return (
    <Router>
      <TopBar />

      <div style={{
        maxWidth: '1040px',
        margin: '0 auto',
        padding: '0 1rem'
      }}>  
        <Switch>
          <Route path='/shipping'>
            <div style={{margin: '40px 0'}} />
          </Route>
          <Route path='/payment'>
            <div style={{margin: '40px 0'}} />
          </Route>
          <Route when='/'>
            <CartHeader />
          </Route>
        </Switch>

        <Row gutter={20}>
          <Col lg={{span: 16, push: 0}} md={{span: 16, push: 4}} sm={{span: 18, push: 3}} xs={{span: 24, push: 0}}>
            <Content />
          </Col>
          <Col lg={{span: 8, push: 0}} md={{span: 16, push: 4}} sm={{span: 18, push: 3}} xs={{span: 24, push: 0}} style={{paddingBottom: '2rem'}}>
            <OrderSummary />
          </Col>
        </Row>
      </div>
    </Router>
  )
}

export default App;