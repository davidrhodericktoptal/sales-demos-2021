import { configureStore } from '@reduxjs/toolkit'
import orderReducer from '../components/orderSlice'

export default configureStore({
  reducer: {
    order: orderReducer
  }
})
