import React from 'react'
import {Row, Col, Typography, Space} from 'antd'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faAngleLeft, faClipboardList, faShareAlt} from '@fortawesome/free-solid-svg-icons'

const {Title} = Typography

const CartHeader = () => (
  <Row gutter={20} style={{padding: '20px 0'}} align={'middle'}>
    <Col md={12}>
      <Title style={{marginBottom: '0'}}>Cart</Title>
    </Col>

    <Col md={12} style={{textAlign: 'right'}}>
      <Space>
        {/* eslint-disable-next-line */}
        <a href="#">
          <FontAwesomeIcon icon={faAngleLeft} /> Continue Shopping
        </a>
        {/* eslint-disable-next-line */}
        <a href="#">
          <FontAwesomeIcon icon={faClipboardList} /> Save to List
        </a>
        {/* eslint-disable-next-line */}
        <a href="#">
          <FontAwesomeIcon icon={faShareAlt} /> Share
        </a>
      </Space>
    </Col>
  </Row>
)

export default CartHeader