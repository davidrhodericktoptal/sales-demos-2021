import React from 'react'
import {useSelector} from 'react-redux'

import Product from './product'

const Cart = () => {
  const {products, deliveryOptions} = useSelector(state => state.order)
  return (
    <>
      {products.map((product) => <Product key={product.id} product={product} deliveryOptions={deliveryOptions} />)}
    </>
  )
}

export default Cart