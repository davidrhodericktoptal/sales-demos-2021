import React from 'react'
import {Steps} from 'antd'

const {Step} = Steps

const CheckoutSteps = ({current}) => (
  <Steps size="small" current={current} style={{marginBottom: '20px'}}>
    <Step title="Shipping" />
    <Step title="Payment" />
    <Step title="Review & Order" />
  </Steps>
)

export default CheckoutSteps