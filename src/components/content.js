import React from 'react'
import {
  Switch,
  Route
} from 'react-router-dom'

import Cart from './cart'
import Shipping from './shipping'
import Payment from './payment'

const Content = () => (
  <Switch>
    <Route path='/payment'>
      <Payment />
    </Route>
    <Route path='/shipping'>
      <Shipping />
    </Route>
    <Route path='/'>
      <Cart />
    </Route>
  </Switch>)

export default Content