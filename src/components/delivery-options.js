import React from 'react'
import {Radio, Typography} from 'antd'
import {useDispatch} from 'react-redux'

import {changeDelivery} from './orderSlice'

const {Title, Paragraph} = Typography

const DeliveryOptions = ({id = 0, delivery}) => {
  const dispatch = useDispatch()
  
  const onChangeDelivery = (selectedDelivery) => {
    dispatch(changeDelivery({id, selectedDelivery}))
  }

  return (
    <Radio.Group onChange={(event) => onChangeDelivery(event.target.value)} name={`product-${id}-delivery`} defaultValue={delivery.selected} value={delivery.selected}>
      {delivery.options.map(({cost, title, description}, index) => (
        <Radio style={{display: 'inline-block', whiteSpace: 'normal'}} key={index} value={index}>
          <div style={{display: 'inline-block', maxWidth: '70%', verticalAlign: 'top'}}>
            {/* <Title level={4}><NumberFormat decimalScale={2} fixedDecimalScale={true} value={cost} displayType={'text'} prefix={'$'}/></Title> */}
            <Title level={4} style={{fontSize: '12px'}}>{title}</Title>
            <Paragraph style={{fontSize: '12px'}}>{description}</Paragraph>
          </div>
        </Radio>
      ))}
    </Radio.Group>
  )
}

export default DeliveryOptions