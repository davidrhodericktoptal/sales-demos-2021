import React from 'react'
import {useSelector} from 'react-redux'
import {Typography} from 'antd'

import DeliveryOptions from './delivery-options'

const {Title, Text} = Typography

const Delivery = () => {
  const {products, deliveryOptions} = useSelector(state => state.order)
  const totalItems = products.reduce((items, {quantity}) => { return items + quantity }, 0)
  return(
    <>
      <Title level={2} style={{fontSize: '16px'}}>Delivery <Text type="secondary" style={{fontSize: '14px'}}>({totalItems} item{totalItems !== 1 && `s`})</Text></Title>

      <DeliveryOptions delivery={deliveryOptions} />
    </>
  )
}

export default Delivery