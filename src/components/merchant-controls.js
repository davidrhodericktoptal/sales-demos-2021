import React from 'react'
import {Space, Typography, Radio, Collapse, Checkbox, Input} from 'antd'
import {useSelector, useDispatch} from 'react-redux'
import {activateProcessor, activateMerchantRule, surchargeProducts, changeMerchantLogo} from './orderSlice'

const {Title} = Typography
const {Panel} = Collapse

const MerchantControls = () => {
  const products = useSelector(state => state.order.products)
  const merchantControls = useSelector(state => state.order.merchantControls)
  const dispatch = useDispatch()
  const surchargedProductsOptions = products.map(({shortTitle, id}) => {
    return {
      label: shortTitle,
      value: id
    }
  })

  const onActivateProcessor = (processor) => {
    dispatch(activateProcessor(processor))
  }

  const onActivateMerchantRule = (merchantRule) => {
    dispatch(activateMerchantRule(merchantRule))
  }

  const onSurchargeProducts = (products) => {
    dispatch(surchargeProducts(products))
  }

  const onChangeMerchantLogo = (logo) => {
    dispatch(changeMerchantLogo(logo))
  }

  const radioGroupStyle = {
    width: '100%'
  }

  const radioButtonStyle = {
    display: 'block',
    width: '100%',
    textAlign: 'center',
    borderLeftWidth: '1px',
    overflow: 'visible',
    padding: '10px'
  }

  const radioStyle = {
    display: 'block',
    marginBottom: '7px'
  }

  return(
    <Space direction="vertical" style={{marginBottom: '2rem'}}>
      <Title level={2}>Processor</Title>

      <Radio.Group style={radioGroupStyle} buttonStyle="solid" defaultValue={merchantControls.activeProcessor} onChange={(event) => onActivateProcessor(event.target.value)}>
        <Radio.Button value="interchange-a" style={{
          ...radioButtonStyle,
          borderRadius: '2px 2px 0 0',
          borderBottomWidth: 0
        }}>Interchange A</Radio.Button>
        <Radio.Button value="interchange-b" style={{
          ...radioButtonStyle,
          borderBottomWidth: 0
        }}>Interchange B</Radio.Button>
        <Radio.Button value="fixed-a" style={{
          ...radioButtonStyle,
          borderBottomWidth: 0
        }}>Fixed Rate A</Radio.Button>
        <Radio.Button value="fixed-b" style={{
          ...radioButtonStyle,
          borderRadius: '0 0 2px 2px'
        }}>Fixed Rate B</Radio.Button>
      </Radio.Group>

      <Title style={{marginTop: '20px'}} level={2}>Merchant Rules</Title>

      <Radio.Group style={radioGroupStyle} defaultValue={merchantControls.activeMerchantRule} onChange={(event) => onActivateMerchantRule(event.target.value)}>
        <Radio value="demo-no-rules" style={radioStyle}>
          <Collapse ghost defaultActiveKey={['1']} activeKey={merchantControls.activeMerchantRule} style={{display: 'inline-block', verticalAlign: 'top'}} collapsible={false}>
            <Panel header="Standard Surcharging" showArrow={false} style={{padding: 0}} key="demo-no-rules">
              <Checkbox.Group options={surchargedProductsOptions} defaultValue={merchantControls.productsSurcharged} onChange={onSurchargeProducts} />
            </Panel>
          </Collapse>
        </Radio>
        <Radio value="demo-discount-rate" style={radioStyle}>Discount Transaction Fee by %</Radio>
        <Radio value="demo-order-value" style={radioStyle}>Set Surcharge Amount by Order Value</Radio>
        <Radio value="demo-static-max-amount" style={radioStyle}>Static Maximum Surcharge Amount</Radio>
        <Radio value="demo-static-max-rate" style={radioStyle}>Static Maximum Surcharge Percentage</Radio>
      </Radio.Group>

      <Title style={{marginTop: '20px'}} level={2}>Merchant Logo</Title>

      <Input type="url" value={merchantControls.merchantLogo} onChange={(event) => onChangeMerchantLogo(event.target.value)} />
    </Space>
  )
}

export default MerchantControls