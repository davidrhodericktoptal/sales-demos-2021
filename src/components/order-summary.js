import React from 'react'
import {Row, Col, Button, Input, Typography, Divider} from 'antd'
import {useSelector} from 'react-redux'
import NumberFormat from 'react-number-format'
import {
  Route,
  Switch,
  Link
} from 'react-router-dom'

import Delivery from './delivery'

const {Title} = Typography

const OrderSummary = () => {
  const {subtotal, delivery, tax, transactionFee, paymentInformation} = useSelector (state => state.order)
  const {postalCode, creditCard} = paymentInformation

  return (
    <>
      <div style={{border: '1px solid #eee', padding: '10px', position: 'relative', marginBottom: '20px'}}>
        <Switch>
          <Route path='/payment'>
            <Link to='/' style={{position: 'absolute', right: '5px'}}>edit cart</Link>
          </Route>
          <Route path='/shipping'>
            <Link to='/' style={{position: 'absolute', right: '5px'}}>edit cart</Link>
          </Route>
          <Route path='/' />
        </Switch>
        
        <Title level={2} style={{textAlign: 'center', fontSize: '20px'}}>Order summary</Title>

        <Row>
          <Col span={12}>
            <Title level={3} style={{fontSize: '16px'}}>Subtotal</Title>
          </Col>
          <Col span={12}>
            <Title level={3} style={{textAlign: 'right', fontSize: '16px'}}><NumberFormat decimalScale={2} fixedDecimalScale={true} value={subtotal} displayType={'text'} prefix={'$'}/></Title>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <Title level={3} style={{fontSize: '16px'}}>Tax</Title>
          </Col>
          <Col span={12}>
            <Title level={3} style={{textAlign: 'right', fontSize: '16px'}}><NumberFormat decimalScale={2} fixedDecimalScale={true} value={tax} displayType={'text'} prefix={'$'}/></Title>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <Title level={3} style={{fontSize: '16px'}}>Delivery</Title>
          </Col>
          <Col span={12}>
            <Title level={3} style={{textAlign: 'right', fontSize: '16px'}}>{delivery > 0 ? <NumberFormat decimalScale={2} fixedDecimalScale={true} value={delivery} displayType={'text'} prefix={'$'}/> : <span style={{color: 'green', fontWeight: 'bold'}}>FREE</span>}</Title>
          </Col>
        </Row>
        {postalCode.length === 5 && creditCard.substr(0, 6).length === 6 && <Row>
          <Col span={12}>
            <Title level={3} style={{fontSize: '16px'}}>Transaction Fee</Title>
          </Col>
          <Col span={12}>
            <Title level={3} style={{textAlign: 'right', fontSize: '16px'}}><NumberFormat decimalScale={2} fixedDecimalScale={true} value={transactionFee} displayType={'text'} prefix={'$'}/></Title>
          </Col>
        </Row>}

        <Row gutter={10}>
          <Col span={16}>
            <Input disabled placeholder="Add Coupon" />
          </Col>
          <Col span={8}>
            <Button block>ADD</Button>
          </Col>
        </Row>

        <Divider />

        <Row>
          <Col span={12}>
            <Title level={3} style={{fontSize: '16px'}}>Total</Title>
          </Col>
          <Col span={12}>
            <Title level={3} style={{textAlign: 'right', fontSize: '16px'}}><NumberFormat decimalScale={2} fixedDecimalScale={true} value={subtotal + delivery + tax + transactionFee} displayType={'text'} prefix={'$'}/></Title>
          </Col>
        </Row>

        <Divider />
        
        <Switch>
          <Route path="/payment">
            <Delivery />
          </Route>
          <Route path="/shipping">
            <Delivery />
          </Route>
          <Route path="/">
            <Button type="primary" block disabled={subtotal === 0}><Link to="/shipping">Checkout</Link></Button>
          </Route>
        </Switch>
      </div>

      <Switch>
        <Route path="/payment">
          <Button type="primary" block>Continue</Button>
        </Route>
        <Route path="/shipping" />
        <Route path="/" />
      </Switch>
    </>
  )
}

export default OrderSummary