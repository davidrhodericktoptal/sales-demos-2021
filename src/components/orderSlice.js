import { createSlice } from '@reduxjs/toolkit'

import copyPaperImg from '../img/copy-paper.jpeg'
import printCartridgesImg from '../img/brother-pc-301.webp'

const initialState = {
  subtotal: 29.99 + 51.49,
  surchargeableSubtotal: 29.99 + 51.49,
  delivery: 0,
  tax: (29.99 + 51.49) * .05,
  transactionFee: 0,
  products: [{
    id: 196517,
    title: 'Biose X-9 Multi-Use Copy Paper, Letter Size (8 1/2" x 11"), 20Lb, Bright White, Ream of 500 Sheets, Case Of 10 Reams',
    shortTitle: 'Biose Copy Paper',
    image: copyPaperImg,
    price: 29.99,
    unit: 'case',
    quantity: 1
  }, {
    id: 610615,
    title: 'Brother PC-301, Black Print Cartridges, Pack Of 2',
    shortTitle: 'Brother Print Cartridges',
    image: printCartridgesImg,
    price: 51.49,
    unit: 'pack',
    quantity: 1
  }],
  deliveryOptions: {
    selected: 0,
    options: [{
      cost: 0,
      title: 'Free Delivery',
      description: 'Next Business Day Delivery if ordered before 5pm'
    }, {
      cost: 10,
      title: 'Same Day Delivery',
      description: 'Order by 5:00pm today'
    }, {
      cost: 5,
      title: 'Store and Curbside Pickup',
      description: 'Ready for pickup today @ Los Gatos, Store #950'
    }]
  },
  merchantControls: {
    activeProcessor: 'interchange-a',
    activeMerchantRule: 'demo-no-rules',
    productsSurcharged: [196517, 610615],
    merchantLogo: 'https://wpadmin.interpayments.com/wp-content/uploads/2020/10/InterPayments-Logo.png'
  },
  paymentInformation: {
    postalCode: '',
    creditCard: ''
  }
}

const orderSlice = createSlice({
  name: 'order',
  initialState,
  reducers: {
    changeQuantity(state, action) {
      const {id, quantity} = action.payload
      const existingProduct = state.products.find(product => product.id === id)
      if(existingProduct) {
        existingProduct.quantity = quantity
      }

      state.subtotal = state.products.reduce((subtotal, {quantity, price}) => {
        return subtotal + quantity * price
      }, 0)

      state.surchargeableSubtotal = state.products.reduce((subtotal, {quantity, price, id}) => {
        if(state.merchantControls.productsSurcharged.indexOf(id) !== -1) {
          return subtotal + quantity * price
        } else {
          return subtotal
        }
      }, 0)

      const totalItems = state.products.reduce((total, {quantity}) => {
        return total + quantity
      }, 0)
      state.delivery = totalItems > 0 ? state.deliveryOptions.options[state.deliveryOptions.selected].cost : 0

      state.tax = state.subtotal * .05
    },
    changeDelivery(state, action) {
      // eslint-disable-next-line
      const {selectedDelivery} = action.payload

      state.deliveryOptions.selected = selectedDelivery

      state.delivery = state.subtotal > 0 ? state.deliveryOptions.options[selectedDelivery].cost : 0
    },
    changePostalCode(state, action) {
      state.paymentInformation.postalCode = action.payload
    },
    activateProcessor(state, action) {
      state.merchantControls.activeProcessor = action.payload
    },
    activateMerchantRule(state, action) {
      state.merchantControls.activeMerchantRule = action.payload
    },
    surchargeProducts(state, action) {
      state.merchantControls.productsSurcharged = action.payload

      state.surchargeableSubtotal = state.products.reduce((subtotal, {quantity, price, id}) => {
        if(state.merchantControls.productsSurcharged.indexOf(id) !== -1) {
          return subtotal + quantity * price
        } else {
          return subtotal
        }
      }, 0)
    },
    updateCreditCard(state, action) {
      state.paymentInformation.creditCard = action.payload
    },
    setTransactionFee(state, action) {
      state.transactionFee = action.payload
    },
    changeMerchantLogo(state, action) {
      state.merchantControls.merchantLogo = action.payload
    }
  }
})

export const {
    changeQuantity,
    changeDelivery,
    changePostalCode,
    activateProcessor,
    activateMerchantRule,
    surchargeProducts,
    updateCreditCard,
    setTransactionFee,
    changeMerchantLogo
  } = orderSlice.actions

export default orderSlice.reducer