import React, {useState, useEffect} from 'react'
import {Row, Col, Form, Input, Typography, Tabs, Image, Space} from 'antd'
import {useDispatch, useSelector} from 'react-redux'
import {updateCreditCard} from './orderSlice'

import CheckoutSteps from './checkout-steps'

import amEx from '../img/american-express-logo.png'
import mastercard from '../img/mastercard-logo.png'
import visa from '../img/visa-logo.png'
import secure from '../img/secure.png'

const {Title} = Typography
const {TabPane} = Tabs

const Payment = () => {
  const {creditCard} = useSelector(state => state.order.paymentInformation)
  const [form] = Form.useForm()
  let displayValue = ''
  const dispatch = useDispatch()
  const [creditCardDisplayValue, setCreditCardDisplayValue] = useState('')

  const setCreditCardInput = (value) => {
    if(value.length === 6) {
      value += 'XXXXXXXXXX'
    }

    const displayValueArray = value.match(/.{1,4}/g)

    if(displayValueArray !== null) {
      if(displayValueArray.length > 1) {
        displayValue = displayValueArray.join(' ')
      } else {
        displayValue = displayValueArray[0]
      }
    } else {
      displayValue = value
    }

    setCreditCardDisplayValue(displayValue)

    dispatch(updateCreditCard(value))
  }

  useEffect(() => {
    setCreditCardInput(creditCard)
    // eslint-disable-next-line
  }, [])

  const creditCardBSDel = (e) => {
    const keycode = e.keyCode || e.charCode
    const value = e.target.value.replace(/\D/g,'').split(' ').join('').substr(0, 6)

    if((keycode === 8 || keycode === 46) && value.length === 6) {
      e.preventDefault()
      setCreditCardInput(value.substr(0, 5))
    }
  }

  const inputCreditCard = (event) => {
    const value = event.target.value.replace(/\D/g,'').split(' ').join('').substr(0, 6)
    setCreditCardInput(value)
  }

  const [fields, setFields] = useState([
    {
      name: ['credit-card'],
      value: creditCardDisplayValue
    }
  ])

  return(
    <>
      <CheckoutSteps current={1} />

      <Title>Choose payment method</Title>

      <Title level={2}>Payment Information</Title>

      <Tabs defaultActiveKey="1" type="card" tabBarStyle={{marginBottom: '0'}} style={{margin: '0 auto 2rem'}}>
        <TabPane tab="Credit card" key="1" style={{border: '1px solid #eee', borderTop: '0', padding: '10px'}}>
          <Form
            form={form}
            layout="vertical"
            fields={fields}
            onChange={(_, newFields) => {
              setFields(newFields)
            }}
          >
            <Row gutter={10}>
              <Col span={16}>
                <Form.Item label="Card Number" required style={{position: 'relative'}}>
                  <Space style={{float: 'right', marginTop: '-1.75rem'}}>
                    <Image style={{maxHeight: '1rem', width: 'auto'}} src={amEx} preview={false} alt="American Express" />
                    <Image style={{maxHeight: '1rem', width: 'auto'}} src={mastercard} preview={false} alt="Mastercard" />
                    <Image style={{maxHeight: '1rem', width: 'auto'}} src={visa} preview={false} 
                    alt="Visa" />
                  </Space>

                  <Input onChange={inputCreditCard} name="credit-card" value={creditCardDisplayValue} onKeyDown={creditCardBSDel}/>
                  
                  <Space style={{float: 'right', marginTop: '-1.8rem', marginRight: '.25rem'}}>
                    <Image style={{maxHeight: '1.5rem', width: 'auto'}} src={secure} preview={false} alt="Secure" />
                  </Space>
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item label="Security Code" required>
                  <Input maxLength="4" disabled value='1234' style={{textAlign: 'right'}}/>
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={10}>
              <Col span={6}>
                <Form.Item label="Expiration Date" required>
                  <Row gutter={10}>
                    <Col span={12}>
                      <Input maxLength="2" disabled value='12' style={{textAlign: 'center'}}/>
                    </Col>

                    <Col span={12}>
                      <Input maxLength="2" disabled value='34' style={{textAlign: 'center'}}/>
                    </Col>
                  </Row>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </TabPane>
        <TabPane tab="PayPal" key="2" disabled/>
      </Tabs>
    </>
  )
}

export default Payment