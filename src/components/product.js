import React from 'react'
import {Row, Col, InputNumber, Checkbox, Image, Typography} from 'antd'
import NumberFormat from 'react-number-format'
import {useDispatch} from 'react-redux'
import {changeQuantity} from './orderSlice'

import DeliveryOptions from './delivery-options'

const {Title, Paragraph} = Typography

const Product = ({product, deliveryOptions}) => {
  const {id, title, image, price, unit, quantity} = product
  const dispatch = useDispatch()

  const onChangeQuantity = (quantity) => {
    dispatch(changeQuantity({id, quantity}))
  }

  return (
    <Row gutter={[30, 30]} style={{border: '1px solid #eee', margin: '0 0 10px', maxWidth: '100%'}}>
      <Col lg={4} md={8} sm={{span: 8, push: 0}} xs={{span: 12, push: 6}}>
        <Image preview={false} placeholder={true} src={image} alt={title} style={{width: 'auto', maxWidth: '100%'}} />
      </Col>
      <Col lg={7} md={16} sm={16}>
        <Title level={2} style={{fontSize: '14px'}}>{title}</Title>
        <Title level={3} style={{fontSize: '12px', fontWeight: 'normal', marginBottom: '16px'}}>Product #{id}</Title>
        
        <Checkbox type="checkbox" disabled>Subscribe</Checkbox>
      </Col>
      <Col lg={6} md={14} sm={24}>
        <DeliveryOptions id={id} delivery={deliveryOptions} />
      </Col>
      <Col lg={3} md={5} sm={12} xs={12}>
        <InputNumber value={quantity} onChange={onChangeQuantity} style={{maxWidth: '100%', textAlign: 'center'}} min={0} precision={0}/>
      </Col>
      <Col lg={4} md={5} sm={12} xs={12} style={{textAlign: 'right'}}>
        <Paragraph style={{fontSize: '10px'}}><NumberFormat decimalScale={2} fixedDecimalScale={true} value={price} displayType={'text'} prefix={'$'}/> / {unit}</Paragraph>
        <Title level={2} style={{fontSize: '16px'}}><NumberFormat decimalScale={2} fixedDecimalScale={true} value={price * quantity} displayType={'text'} prefix={'$'}/></Title>
      </Col>
      <Col lg={24} style={{textAlign: 'right'}}>
        {/* eslint-disable-next-line */}
        <a href="#">Save for later</a>
      </Col>
    </Row>
  )
}

export default Product