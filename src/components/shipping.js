import React, {useState} from 'react'
import {Row, Col, Form, Input, Typography, Button, Checkbox} from 'antd'
import {useHistory} from 'react-router'
import {useSelector, useDispatch} from 'react-redux'

import {changePostalCode} from './orderSlice'

import CheckoutSteps from './checkout-steps'

const {Title} = Typography

const Shipping = () => {
  // eslint-disable-next-line
  let history = useHistory()

  const postalCode = useSelector(state => state.order.paymentInformation.postalCode)
  const dispatch = useDispatch()

  const [fields, setFields] = useState([
    {
      name: ['postal-code'],
      value: postalCode
    }
  ])

  const onChangePostalCode = (postalCode) => {
    dispatch(changePostalCode(postalCode.replace(/[^0-9.]+/g, '')))
  }

  const onFinish = () => {
    history.push('/payment')
  }

  return(
    <>
      <CheckoutSteps current={0} />

      <Title>Shipping Information</Title>

      <Form
        layout='vertical'
        onFinish={onFinish}
        fields={fields}
        onChange={(_, newFields) => {
          setFields(newFields)
        }}
      >
        <Row gutter={10}>
          <Col span={12}>
            <Form.Item label="First Name:" required>
              <Input value="John" disabled />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="Last Name:" required>
              <Input value="Smith" disabled />
            </Form.Item>
          </Col>
        </Row>

        <Form.Item label="Company Name:">
          <Input value="" disabled />
        </Form.Item>

        <Form.Item label="Address:" required>
          <Input value="123 Main Street, Suite 45" disabled />
        </Form.Item>

        <Form.Item label="Postal Code:" name="postal-code" rules={[{ required: true, message: 'Please input a valid postal code', min: 5 }]}>
          <Input maxLength={5} onChange={(event) => onChangePostalCode(event.target.value)} />
        </Form.Item>

        <Form.Item label="Phone:" required>
          <Row gutter={10}>
            <Col span={4}>
              <Input value="+1 (123)" disabled />
            </Col>
            <Col span={8}>
              <Input value="456-7890" disabled />
            </Col>
            <Col span={5}>
            <Input value="1234" disabled />
            </Col>
          </Row>
        </Form.Item>

        <Form.Item label="Phone:" required>
          <Checkbox type="checkbox" disabled>Yes, I would like to receive communications, including exclusive discounts and offers via mail and email</Checkbox>
        </Form.Item>

        <div style={{textAlign: 'right', marginBottom: '2rem'}}>
          <Button type={'primary'} htmlType="submit">Continue</Button>
        </div>
      </Form>
    </>
  )
}

export default Shipping