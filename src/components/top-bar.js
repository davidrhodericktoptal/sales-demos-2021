import React, {useState} from 'react'
import {Row, Col, Image, Space, Tooltip, Button, Drawer} from 'antd'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCommentAlt, faPhoneAlt, faCogs} from '@fortawesome/free-solid-svg-icons'
import {useSelector} from 'react-redux'

import MerchantControls from './merchant-controls'

const TopBar = () => {
  const {merchantLogo} = useSelector(state => state.order.merchantControls)
  const [visible, setVisible] = useState(false)

  const showDrawer = () => {
    setVisible(true)
  }

  const onClose = () => {
    setVisible(false)
  }

  return (
    <Row style={{padding: '10px', borderBottom: '1px solid #eee'}} align="middle">
      <Col sm={12} xs={24}>
        <div style={{display: 'flex', alignItems: 'middle'}}>
          <Image placeholder={false} preview={false} src={merchantLogo} alt="Merchant Logo" style={{maxHeight: '2rem', width: 'auto'}} />
        </div>
      </Col> 
      <Col sm={12} xs={24} style={{textAlign: 'right'}}>
        <Space>
          {/* eslint-disable-next-line */}
          <a href="#"><FontAwesomeIcon icon={faCommentAlt} /> Live Chat</a>
          {/* eslint-disable-next-line */}
          <a href="#"><FontAwesomeIcon icon={faPhoneAlt} /> 1.888.444.5555</a>
          <Tooltip placement="left" title="Open Merchant Controls">
            <Button type="primary" onClick={showDrawer}>
              <FontAwesomeIcon icon={faCogs} style={{cursor: 'pointer'}} />
            </Button>
          </Tooltip>
        </Space>

        <Drawer
          title="Merchant Controls"
          placement="right"
          closable={true}
          onClose={onClose}
          visible={visible}
          width={320}
        >
          <MerchantControls />
        </Drawer>
      </Col>
    </Row>
  )
}

export default TopBar